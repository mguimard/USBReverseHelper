#ifndef STRINGUTILS_H
#define STRINGUTILS_H

#include <string>
#include <vector>

class StringUtils{
public:
    static std::string join(const std::vector<std::string> &lst, const std::string &delim)
    {
        std::string ret;

        for(const auto &s : lst)
        {
            if(!ret.empty())
            {
                ret += delim;
            }
            ret += s;
        }

        return ret;
    }

    static std::string hexa_string(uint8_t byte)
    {
        char str[3];
        sprintf(str, "%02X", byte);
        return str;
    }

    static std::string hexa_string_16(uint16_t i)
    {
        char str[9];
        sprintf(str, "0x%04X", i);
        return str;
    }

    static std::string ascii_string(uint8_t byte)
    {
        if(byte < 32 || byte > 127)
        {
            return "·";
        }

        std::string ret;

        ret = byte;
        // printf("%d => [%s] (%ld)\n", byte, ret.c_str(), ret.size());

        return ret;
    }

    static std::string bytes_to_string(std::vector<uint8_t> bytes)
    {
        std::string ret;

        for(unsigned int i = 0; i < bytes.size();i++)
        {
            ret.append(hexa_string(bytes[i]));
        }

        return ret;
    }
};

#endif // STRINGUTILS_H
