#ifndef COLORUTILS_H
#define COLORUTILS_H

#include <string>
#include <vector>

typedef unsigned int RGBColor;

#define RGBGetRValue(rgb)   (rgb & 0x000000FF)
#define RGBGetGValue(rgb)   ((rgb >> 8) & 0x000000FF)
#define RGBGetBValue(rgb)   ((rgb >> 16) & 0x000000FF)

#define ToRGBColor(r, g, b) ((RGBColor)((b << 16) | (g << 8) | (r)))

class ColorUtils
{
public:

    /*---------------------------------------------------------*\
    | Generate color palette                                    |
    \*---------------------------------------------------------*/
    static std::vector<RGBColor> generate_palette(int n)
    {
        std::vector<RGBColor> palette;

        float step = 300.0/n;

        for(int i = 0; i < n; i ++)
        {
            RGBColor c = HsvToRgb(i * step, 255, 128);
            palette.push_back(c);
        }

        return palette;
    }

    static RGBColor HsvToRgb(int h, int s, int v)
    {
        uint8_t r;
        uint8_t g;
        uint8_t b;
        unsigned char region, remainder, p, q, t;

        if (s == 0)
        {
            r = v;
            g = v;
            b = v;
            return ToRGBColor(r,g,b);
        }

        region = h / 43;
        remainder = (h - (region * 43)) * 6;

        p = (v * (255 - s)) >> 8;
        q = (v * (255 - ((s * remainder) >> 8))) >> 8;
        t = (v * (255 - ((s * (255 - remainder)) >> 8))) >> 8;

        switch (region)
        {
            case 0:
                r = v; g = t; b = p;
                break;
            case 1:
                r = q; g = v; b = p;
                break;
            case 2:
                r = p; g = v; b = t;
                break;
            case 3:
                r = p; g = q; b = v;
                break;
            case 4:
                r = t; g = p; b = v;
                break;
            default:
                r = v; g = p; b = q;
                break;
        }

        return ToRGBColor(r,g,b);
    }

};

#endif // COLORUTILS_H
