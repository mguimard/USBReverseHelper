variables:
  GIT_SUBMODULE_STRATEGY: recursive
  GIT_DEPTH: 1

.shared_windows_runners:
  tags:
  - shared-windows
  - windows
  - windows-1809

stages:
  - build

before_script:
 - echo "started by ${GITLAB_USER_NAME}"

#reusable templates
.ccache_init: &ccache_init
  before_script:
    - export QT_SELECT=qt5
    - export APPIMAGE_EXTRACT_AND_RUN=1

#-----------------------------------------------------------------------#
# Linux (AppImage) 64-bit Build Target                                  #
#-----------------------------------------------------------------------#
"Linux 64 AppImage":
  <<: *ccache_init
  image: registry.gitlab.com/openrgbdevelopers/openrgb-linux-ci-deb-builder:bullseye-64
  stage: build
  script:
  - apt install libpcap-dev -y
  - export $(dpkg-architecture)
  - ./scripts/build-appimage.sh

  artifacts:
    name: "${CI_PROJECT_NAME}_Linux_64_${CI_COMMIT_SHORT_SHA}"
    paths:
    - USBReverseHelper-x86_64.AppImage
    - README.md
    expire_in: 30 days

#-----------------------------------------------------------------------#
# Windows (64-bit) Build Target                                         #
#-----------------------------------------------------------------------#
"Windows 64":
  extends:
  - .shared_windows_runners
  stage: build
  script:
  - $esc = "$([char]27)"
  - $count = 0
  - function _unix_tmsec_ { [int64](([datetime]::UtcNow)-(get-date "1/1/1970")).TotalSeconds }
  - function _fold_start_ { param( [string]$TEXT_TAG ) $t=_unix_tmsec_; $global:count += 1; Write-Host -NoNewLine "`r`n`r`nsection_start:${t}:sect_${count}`r${esc}[0K${esc}[33m${TEXT_TAG}${esc}[39m`r`n"; }
  - function _fold_final_ {                            $t=_unix_tmsec_;                     Write-Host -NoNewLine   "`r`n`r`nsection_end:${t}:sect_${count}`r${esc}[0K`r`n"           ; }


  - _fold_start_ 'configuring the msvc environment variables'
  - Push-Location "C:/Program Files (x86)/Microsoft Visual Studio/2019/BuildTools/VC/Auxiliary/Build"
  - '& cmd.exe /C "vcvarsall.bat x64 & set" | Foreach-Object { if ($_ -match "(.*?)=(.*)") { Set-Item -force -path "Env:\$($matches[1])" -value "$($matches[2])" } }'
  - Pop-Location
  - _fold_final_

  - _fold_start_ 'downloading precompiled versions of qtbase, qttools (for windeployqt) and jom (for a more parallel nmake)'
  - mkdir _qt
  - mkdir _qt_download
  - Push-Location _qt_download
  - curl.exe -LJ -o qt-base.7z  'https://qt-mirror.dannhauer.de/online/qtsdkrepository/windows_x86/desktop/qt5_5150/qt.qt5.5150.win64_msvc2019_64/5.15.0-0-202005150700qtbase-Windows-Windows_10-MSVC2019-Windows-Windows_10-X86_64.7z'
  - curl.exe -LJ -o qt-tools.7z 'https://qt-mirror.dannhauer.de/online/qtsdkrepository/windows_x86/desktop/qt5_5150/qt.qt5.5150.win64_msvc2019_64/5.15.0-0-202005150700qttools-Windows-Windows_10-MSVC2019-Windows-Windows_10-X86_64.7z'
  - curl.exe -LJ -o qt-jom.zip  'https://qt-mirror.dannhauer.de/official_releases/jom/jom.zip'
  - _fold_final_

  - _fold_start_ 'extracting the downloaded qt binaries'
  - 7z x qt-base.7z  '-o../_qt' -y
  - 7z x qt-tools.7z '-o../_qt' -y
  - 7z x qt-jom.zip  '-o../_qt' -y
  - _fold_final_

  - _fold_start_ 'turn the qt install from enterprise to foss; remove the licensing checks'
  - ${qconfig-pri-folder} = '..\_qt\5.15.0\msvc2019_64\mkspecs\qconfig.pri'
  - (Get-Content ${qconfig-pri-folder}).replace('QT_EDITION = Enterprise', 'QT_EDITION = OpenSource') | Set-Content ${qconfig-pri-folder}
  - (Get-Content ${qconfig-pri-folder}).replace('QT_LICHECK = licheck.exe', '')                       | Set-Content ${qconfig-pri-folder}
  - Pop-Location
  - _fold_final_

  - _fold_start_ 'run qmake and generate the msvc nmake makefile'
  - mkdir _build; cd _build
  - ..\_qt\5.15.0\msvc2019_64\bin\qmake ..\USBReverseHelper.pro
  - _fold_final_

  - _fold_start_ 'start the actual build with jom instead of nmake; for speed'
  - ..\_qt\jom
  - _fold_final_

  - _fold_start_ 'run windeployqt to automatically copy the needed dll files'
  - ..\_qt\5.15.0\msvc2019_64\bin\windeployqt --no-angle --no-translations --no-opengl-sw --no-system-d3d-compiler --no-compiler-runtime --no-webkit2 .\release\
  - _fold_final_

  - _fold_start_ 'Moving results for upload'
  - Get-Location
  - mv ../dependencies/winpcap/Lib/x64/*.dll release/
  - rm release/*.obj
  - rm release/*.cpp
  - rm release/*.h
  - mv release ../'USBReverseHelper Windows 64-bit'
  - _fold_final_
#  cache:
#    key: same-key
#    paths:
#    - C:\vcpkg\installed\
  artifacts:
    name: "${CI_PROJECT_NAME}_Windows_64_${CI_COMMIT_SHORT_SHA}"
    paths:
    - 'USBReverseHelper Windows 64-bit'
    expire_in: 30 days

