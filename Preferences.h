#ifndef PREFERENCES_H
#define PREFERENCES_H

#include "session.h"
#include <set>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include "nlohmann/json.hpp"


using json = nlohmann::json;

class Preferences
{

public:
    static bool Save(std::string filename, Session* session)
    {
        std::ofstream file(filename, std::ios::out | std::ios::binary);

        if(file)
        {
            try
            {
                const json j = session->ToJSON();
                file << j.dump(4);
                file.close();
            }
            catch(const std::exception& e)
            {
                printf("Cannot write file: %s\n", e.what());
                return false;
            }
        }

        return true;
    }

    static Session* Load(std::string filename)
    {
        json j;

        std::ifstream file(filename);

        if(file)
        {
            try
            {
                file >> j;
                file.close();

                return Session::FromJSON(j);
            }
            catch(const std::exception& e)
            {
                 printf("Cannot read file: %s\n", e.what());
                 throw e;
            }
        }
        else
        {
            throw "Cannot load file, does not exist";
        }
    }

    static bool AddToRecentSessions(std::string executable_dir, std::string session_file_name)
    {

        std::set<std::string> recentSessions;
        try
        {
            recentSessions = GetRecentSessions(executable_dir);
        }
        catch(const std::exception& e)
        {
            printf("No recent sessions found.\n");
        }

        recentSessions.insert(session_file_name);

        std::ofstream file(executable_dir + "recent-sessions.json", std::ios::out | std::ios::binary);

        if(file)
        {
            try
            {
                const json j = recentSessions;
                file << j.dump(4);
                file.close();
            }
            catch(const std::exception& e)
            {
                printf("Cannot write file: %s\n", e.what());
                return false;
            }
        }

        return true;
    }

    static std::set<std::string> GetRecentSessions(std::string executable_dir)
    {
        json j;

        std::ifstream file(executable_dir + "recent-sessions.json");

        if(file)
        {
            try
            {
                file >> j;
                file.close();

                return j;
            }
            catch(const std::exception& e)
            {
                 printf("Cannot read file: %s\n", e.what());
                 throw e;
            }
        }
        else

        {
            throw std::invalid_argument("Cannot load file, does not exist");
        }
    }

    static std::string LoadDocumentation(std::string folder)
    {
         std::ifstream file(folder + "/documentation.md");

         if(file)
         {
             try
             {
                 std::string doc;
                 std::string line;

                 while (getline(file, line))
                 {
                     doc.append(line);
                     doc.append("\n");
                 }

                 file.close();

                 return doc;
             }
             catch(const std::exception& e)
             {
                  printf("Cannot read file: %s\n", e.what());
                  return "";
             }
         }
         else
         {
             printf("Cannot load document file, does not exist\n");
             return "";
         }
    }

    static bool SaveDocumentation(std::string folder, std::string content)
    {
        std::ofstream file(folder + "/documentation.md", std::ios::out | std::ios::binary);

        if(file)
        {
            try
            {
                file << content;
                file.close();
            }
            catch(const std::exception& e)
            {
                printf("Cannot write file: %s\n", e.what());
                return false;
            }
        }

        return true;
    }

};

#endif // PREFERENCES_H
