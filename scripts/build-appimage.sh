#!/bin/bash

set -x
set -e

TEMP_BASE=/tmp
BUILD_DIR=$(mktemp -d -p "$TEMP_BASE" appimage-build-XXXXXX)

if [ ${DEB_HOST_ARCH:0:1} == ${DEB_HOST_GNU_CPU:0:1} ]; then
    ARCH="$DEB_HOST_ARCH"
else
    ARCH="$DEB_HOST_GNU_CPU"
fi
echo Inputs: "$DEB_HOST_ARCH" "$DEB_HOST_GNU_CPU"
echo Calculated: "$ARCH"

TARGET=USBReverseHelper

cleanup () {
    if [ -d "$BUILD_DIR" ]; then
        rm -rf "$BUILD_DIR"
    fi
}
trap cleanup EXIT

REPO_ROOT=$(readlink -f $(dirname $(dirname $0)))
OLD_CWD=$(readlink -f .)

pushd "$BUILD_DIR"

qmake "$REPO_ROOT"

make -j$(nproc) TARGET="$TARGET"
make install INSTALL_ROOT=AppDir

export QML_SOURCES_PATHS="$REPO_ROOT"/src

linuxdeploy-"$ARCH".AppImage --appdir AppDir -e "$TARGET" -i "$REPO_ROOT"/res/USBReverseHelper.png -d "$REPO_ROOT"/qt/USBReverseHelper.desktop
linuxdeploy-plugin-qt-"$ARCH".AppImage --appdir AppDir
linuxdeploy-"$ARCH".AppImage --appdir AppDir --output appimage

mv -v "$TARGET"*.AppImage "$OLD_CWD"
