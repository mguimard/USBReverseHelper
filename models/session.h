#ifndef SESSION_H
#define SESSION_H

#include "analysis.h"
#include <string>
#include <vector>

#include "nlohmann/json.hpp"
using json = nlohmann::json;

class Session
{
public:
    Session(std::string);

    json ToJSON();
    static Session* FromJSON(json j);

    std::string GetRootDir();

    std::vector<Analysis*> GetAnalyses();
    void AddAnalysis(Analysis*);
    void RemoveAnalysis(Analysis*);
    void SetDocumentation(std::string doc);
    std::string GetDocumentation();

private:
    Session(){};

    std::string              root_dir;
    std::vector<Analysis*>   analyses;
    std::string documentation;
};

#endif // SESSION_H
