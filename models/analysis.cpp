#include "analysis.h"

Analysis::Analysis(std::vector<std::string> file_set):
    file_set(file_set)
{

}

json Analysis::ToJSON()
{
    json j;

    j["file_set"] = file_set;

    return j;
}

Analysis* Analysis::FromJSON(json j)
{
    Analysis* a = new Analysis();

    a->file_set = j["file_set"];

    return a;
}

void Analysis::AddFile(std::string file)
{
    file_set.push_back(file);
}

void Analysis::RemoveFile(std::string file)
{
    std::vector<std::string>::iterator itr = std::find(file_set.begin(), file_set.end(), file);
    if (itr != file_set.end()) file_set.erase(itr);
}


std::vector<std::string> Analysis::GetFiles()
{
    return file_set;
}

bool Analysis::HasFiles()
{
    return !file_set.empty();
}
