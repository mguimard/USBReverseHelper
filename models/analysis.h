#ifndef ANALYSIS_H
#define ANALYSIS_H

#include "nlohmann/json.hpp"

#include <string>
#include <vector>

using json = nlohmann::json;

class Analysis
{
public:
    Analysis(std::vector<std::string>);

    json ToJSON();
    void AddFile(std::string);
    void RemoveFile(std::string);
    static Analysis* FromJSON(json j);
    std::vector<std::string> GetFiles();
    bool HasFiles();

private:
    Analysis(){};
    std::vector<std::string> file_set;


};


#endif // ANALYSIS_H
