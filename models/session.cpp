﻿#include "session.h"

Session::Session(std::string dir) : root_dir(dir) {}

json Session::ToJSON()
{
    json j;

    j["root_dir"] = root_dir;

    std::vector<json> analyses_json;

    for(Analysis* a: analyses)
    {
        analyses_json.push_back(a->ToJSON());
    }

    j["analyses"] = analyses_json;

    return j;
}

Session* Session::FromJSON(json j)
{
    Session* s = new Session();

    s->root_dir = j["root_dir"];

    for(const json& a : j["analyses"])
    {
        s->analyses.push_back(Analysis::FromJSON(a));
    }

    return s;
}

void Session::AddAnalysis(Analysis* analysis)
{
    analyses.push_back(analysis);
}

void Session::RemoveAnalysis(Analysis* analysis)
{
    analyses.erase(std::remove(analyses.begin(), analyses.end(), analysis), analyses.end());
}

std::string Session::GetRootDir()
{
    return root_dir;
}

std::vector<Analysis*> Session::GetAnalyses()
{
    return analyses;
}

void Session::SetDocumentation(std::string doc)
{
    documentation = doc;
}

std::string Session::GetDocumentation()
{
    return documentation;
}
