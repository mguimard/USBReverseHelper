# Contributing guide

We love contributions. Feel free to discuss about a new feature in the issues.

## Workflow

Fork this project and clone your fork.

```
git checkout -b my-new-feature
// work ...
git add --all
git commit -m "Some commit message here..."
git push origin my-new-feature
// Open a merge request.
```

