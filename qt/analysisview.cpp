#include "analysisview.h"
#include "ui_analysisview.h"

#include "StringUtils.h"
#include "packetdetailsview.h"

#include <QStandardItemModel>
#include <QFileDialog>
#include <QPushButton>
#include <QTimer>
#include <QApplication>
#include <QClipboard>
#include <set>
#include <regex>

AnalysisView::AnalysisView(QWidget *parent, QString fileName) :
    QWidget(parent),
    ui(new Ui::AnalysisView)
{
    analysis = new Analysis({fileName.toStdString()});
    Setup();
}

AnalysisView::AnalysisView(QWidget *parent, Analysis * analysis) :
    QWidget(parent),
    ui(new Ui::AnalysisView),
    analysis(analysis)
{
    Setup();
}

void AnalysisView::Setup()
{
    ui->setupUi(this);
    ui->display->addItem("HEX");
    ui->display->addItem("TEXT");
    ui->tableView->hide();
    ui->options->hide();

    /*---------------------------------------------------------*\
    | Automatically load and parse the pcapng file if exists    |
    \*---------------------------------------------------------*/
    if(analysis->GetFiles().size() > 0)
    {
        qRegisterMetaType<std::vector<ParsedPacket>>("std::vector<ParsedPacket>");
        ParserWorker *worker = new ParserWorker(analysis->GetFiles().front());
        worker->moveToThread(&workerThread);
        connect(&workerThread, &QThread::finished, worker, &QObject::deleteLater);
        connect(worker, &ParserWorker::ResultReady, this, &AnalysisView::OnParserResult);
        workerThread.start();
        QTimer::singleShot(100, worker, &ParserWorker::run);
    }
    else
    {
        OnParserResult({});
    }
}

AnalysisView::~AnalysisView()
{
    workerThread.quit();
    workerThread.wait();
    delete analysis;
    delete ui;
}

void AnalysisView::OnParserResult(std::vector<ParsedPacket> result)
{
    ui->loading->hide();
    ui->options->show();
    ui->tableView->show();

    packets = result;

    QStringList dst_devices_list;

    for(ParsedPacket& packet: packets)
    {
        if(!packet.dst.empty())
        {
            dst_devices_list << QString::fromStdString(packet.dst);
        }
    }

    proxyModel = new PacketsSortFilterProxyModel(packets);
    UpdatePacketFilters();

    /*---------------------------------------------------------*\
    | Fill in the dst devices filter list                       |
    \*---------------------------------------------------------*/
    ui->dst->clear();
    dst_devices_list.removeDuplicates();
    ui->dst->addItems(dst_devices_list);

    CreateTableView();
}

Analysis* AnalysisView::GetAnalysis()
{
    return analysis;
}

void AnalysisView::CreateTableView()
{
    /*---------------------------------------------------------*\
    | Table view creation                                       |
    \*---------------------------------------------------------*/
    int rows                    = packets.size();
    int columns                 = 0;

    /*---------------------------------------------------------*\
    | Calculate columns count                                   |
    \*---------------------------------------------------------*/
    for(ParsedPacket& packet: packets)
    {
        columns = std::max<int>(packet.data_len, columns);
    }

    model = new QStandardItemModel(rows, columns + VERTICAL_HEADER_COUNT, this);

    /*---------------------------------------------------------*\
    | Create horizontal headers                                 |
    \*---------------------------------------------------------*/
    for(int col = 0; col < columns + VERTICAL_HEADER_COUNT; col++)
    {
        if(col == FRAME_COL_INDEX)
        {
            model->setHeaderData(FRAME_COL_INDEX, Qt::Horizontal, "#");
        }
        else if(col == TIME_COL_INDEX)
        {
            model->setHeaderData(TIME_COL_INDEX, Qt::Horizontal, "Time");
        }
        else if(col == SIZE_COL_INDEX)
        {
            model->setHeaderData(SIZE_COL_INDEX, Qt::Horizontal, "Len");
        }
        else if(col == TYPE_COL_INDEX)
        {
            model->setHeaderData(TYPE_COL_INDEX, Qt::Horizontal, "Info");
        }
        else if(col == COMMENTS_COL_INDEX)
        {
            model->setHeaderData(COMMENTS_COL_INDEX, Qt::Horizontal, "Comments");
        }
        else
        {
            model->setHeaderData(col, Qt::Horizontal, col - VERTICAL_HEADER_COUNT);
        }
    }

    for(int row = 0; row < rows; row++)
    {
        model->setHeaderData(row, Qt::Vertical, "•");
    }

    /*---------------------------------------------------------*\
    | Fill in the table                                         |
    \*---------------------------------------------------------*/
    if(rows > 0)
    {
        timespec first_frame_ts = packets[0].ts;

        for(int row = 0; row < rows; row++)
        {
            const ParsedPacket& packet = packets[row];

            /*---------------------------------------------------------*\
            | Custom columns                                            |
            \*---------------------------------------------------------*/
            timespec frame_ts = packet.ts;
            float diff = (frame_ts.tv_sec + frame_ts.tv_nsec/ 1000000000.0 ) - (first_frame_ts.tv_sec  + first_frame_ts.tv_nsec/ 1000000000.0 );

            model->setData(model->index(row, FRAME_COL_INDEX,       QModelIndex()), packet.frame);
            model->setData(model->index(row, TIME_COL_INDEX,        QModelIndex()), diff);
            model->setData(model->index(row, SIZE_COL_INDEX,        QModelIndex()), packet.data_len);
            model->setData(model->index(row, TYPE_COL_INDEX,        QModelIndex()), packet.get_urb_function_name().c_str());
            model->setData(model->index(row, COMMENTS_COL_INDEX,    QModelIndex()), packet.comment.c_str());

            /*---------------------------------------------------------*\
            | Packet data                                               |
            \*---------------------------------------------------------*/
            for(unsigned int col = 0; col < packet.data.size(); col++)
            {
                QModelIndex index = model->index(row, col + VERTICAL_HEADER_COUNT, QModelIndex());

                if(col < packet.data.size())
                {
                    model->setData(index, StringUtils::hexa_string(packet.data[col]).c_str());
                }
                else
                {
                    model->setData(index, "");
                }
            }
        }
    }

    /*---------------------------------------------------------*\
    | Set the model at the end, saves precious load time        |
    \*---------------------------------------------------------*/
    ui->tableView->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    proxyModel->setSourceModel(model);
    ui->tableView->setModel(proxyModel);
    ui->tableView->setSortingEnabled(true);
    ui->tableView->sortByColumn(0, Qt::AscendingOrder);

    ui->tableView->setColumnWidth(FRAME_COL_INDEX,      40);
    ui->tableView->setColumnWidth(TIME_COL_INDEX,       100);
    ui->tableView->setColumnWidth(SIZE_COL_INDEX,       40);
    ui->tableView->setColumnWidth(TYPE_COL_INDEX,       220);
    ui->tableView->setColumnWidth(COMMENTS_COL_INDEX,   220);

    for(int col = 0; col < columns; col++)
    {
        ui->tableView->setColumnWidth(col + VERTICAL_HEADER_COUNT, 36);
    }

    for(int row = 0; row < rows; row++)
    {
        ui->tableView->setRowHeight(row, 10);
    }

    connect(ui->tableView->verticalHeader(), SIGNAL(sectionDoubleClicked(int)), this, SLOT(OnVerticalHeaderDoubleClicked(int)));
}

void AnalysisView::on_display_currentIndexChanged(int value)
{
    int rows = packets.size();

    if(rows > 0)
    {
        for(int row = 0; row < rows; row++)
        {
            const ParsedPacket& packet = packets[row];
            for(unsigned int col = 0; col < packet.data.size(); col++)
            {
                QModelIndex index = model->index(row, col + VERTICAL_HEADER_COUNT, QModelIndex());

                if(col < packet.data.size())
                {
                    if(value == 0)
                    {
                        model->setData(index, StringUtils::hexa_string(packet.data[col]).c_str());
                    }
                    else if (value == 1)
                    {
                        model->setData(index, StringUtils::ascii_string(packet.data[col]).c_str());
                    }

                }
                else
                {
                    model->setData(index, "");
                }
            }
        }
    }
}

void AnalysisView::UpdatePacketFilters()
{
    proxyModel->filters.writes_only = ui->writes_only->isChecked();
    proxyModel->filters.comments_only = ui->comments_only->isChecked();
    proxyModel->filters.size_filter = ui->packet_size_filter->isChecked();
    proxyModel->filters.size = ui->packet_size->value();
    proxyModel->filters.search = ui->search->text().trimmed().toStdString();
    proxyModel->filters.selected_dst_devices.clear();

    for(QString& dst:ui->dst->currentText())
    {
        proxyModel->filters.selected_dst_devices.insert(dst.toStdString());
    }
}

void AnalysisView::ApplyFilters()
{
    UpdatePacketFilters();
    proxyModel->invalidate();
    ApplyColumnsPalettes(ui->colorize->isChecked());
}

void AnalysisView::on_tableView_doubleClicked(const QModelIndex & index)
{
    /*---------------------------------------------------------*\
    | Filter on packet size                                     |
    \*---------------------------------------------------------*/
    if(index.column() == SIZE_COL_INDEX)
    {
        int data_len = index.data().toInt();;
        ui->packet_size_filter->setChecked(true);
        ui->packet_size->setValue(data_len);
    }
}

void AnalysisView::ApplyColumnsPalettes(bool show_colors)
{
    /*---------------------------------------------------------*\
    | Reset colors                                              |
    \*---------------------------------------------------------*/
    if(!show_colors)
    {
        for(unsigned int row = 0; row < packets.size(); row++)
        {
            for(unsigned int col = 0; col < packets[row].data_len; col++)
            {
                model->item(row, col + VERTICAL_HEADER_COUNT)->setData(QBrush(Qt::transparent), Qt::BackgroundRole);
            }
        }

        return;
    }

    /*---------------------------------------------------------*\
    | Share palettes accross columns                            |
    \*---------------------------------------------------------*/
    std::vector<ParsedPacket> filtered_packets = proxyModel->GetFilteredPackets();

    unsigned int columns = 0;

    for(ParsedPacket& packet: filtered_packets)
    {
        columns = std::max<unsigned int>(packet.data_len, columns);
    }

    for(unsigned int col = 0; col < columns; col++)
    {
        std::set<uint8_t> values;

        for(unsigned int row = 0; row < filtered_packets.size(); row++)
        {
            if(col < filtered_packets[row].data_len)
            {
                values.insert(filtered_packets[row].data[col]);
            }
        }

        if(values.size() > 1)
        {
            std::vector<RGBColor> palette = ColorUtils::generate_palette(values.size());
            std::map<uint8_t, RGBColor> colors;
            std::vector<uint8_t> values_vec(values.begin(), values.end());

            for(unsigned int i = 0; i < values.size(); i++)
            {
                uint8_t v = values_vec[i];
                colors[v] = palette[i];
            }

            for(unsigned int row = 0; row < filtered_packets.size(); row++)
            {
                if(col < filtered_packets[row].data_len)
                {
                    if(filtered_packets[row].data[col] != 0x00) // do not colorize 0x00
                    {
                        const RGBColor& color = colors[filtered_packets[row].data[col]];
                        QColor c = QColor::fromRgb
                                (
                                    RGBGetRValue(color),
                                    RGBGetGValue(color),
                                    RGBGetBValue(color)
                                    );
                        model->item(filtered_packets[row].frame - 1, col + VERTICAL_HEADER_COUNT)->setData(QBrush(c), Qt::BackgroundRole);
                    }
                }
            }
        }
    }
}

void AnalysisView::HighlightSearchMatches(const QString& s)
{
    int rows = proxyModel->rowCount();
    int cols = proxyModel->columnCount();

    QBrush highlight(QColor("#FF00FF"));

    for (int row = 0; row < rows; row++)
    {
        for (int col = VERTICAL_HEADER_COUNT; col < cols; col++)
        {
            QString content = proxyModel->data(proxyModel->index(row, col), Qt::EditRole).toString();

            if(!content.isEmpty() && s.contains(content, Qt::CaseInsensitive))
            {
                proxyModel->setData(proxyModel->index(row, col),highlight, Qt::BackgroundRole);
            }
            else
            {
                proxyModel->setData(proxyModel->index(row, col), QBrush(Qt::transparent), Qt::BackgroundRole);
            }
        }
    }
}

void AnalysisView::ShowPacketDetails(int i)
{
    QDialog dialog(this);

    dialog.setModal(false);
    dialog.setWindowTitle("Packet details");
    PacketDetailsView details_view(packets[i], &dialog);
    QHBoxLayout buttons_layout(&dialog);
    dialog.layout()->addWidget(&details_view);

    dialog.setMinimumSize(details_view.sizeHint().width(),details_view.sizeHint().height());

    dialog.exec();
}

void AnalysisView::HandleCopy()
{
    QAbstractItemModel * model = ui->tableView->model();
    QItemSelectionModel * selection = ui->tableView->selectionModel();
    QModelIndexList indexes = selection->selectedIndexes();

    QString str;
    QModelIndex previous = indexes.first();

    foreach(const QModelIndex &current, indexes)
    {
        str.append(current.row() != previous.row() ? "\n" : " ");
        str.append(model->data(current).toString());
        previous = current;
    }

    QApplication::clipboard()->setText(str.trimmed());
}

/*---------------------------------------------------------*\
| GUI bindings                                              |
\*---------------------------------------------------------*/
void AnalysisView::OnVerticalHeaderDoubleClicked(int row)
{
    int frame = proxyModel->data(proxyModel->index(row,0)).toInt();
    ShowPacketDetails(frame-1);
}

void AnalysisView::on_packet_size_filter_stateChanged(int state)
{
    ui->packet_size->setEnabled(state);
    ApplyFilters();
}

void AnalysisView::on_packet_size_valueChanged(int)
{
    ApplyFilters();
}

void AnalysisView::on_dst_selectionChanged()
{
    ApplyFilters();
}

void AnalysisView::on_writes_only_stateChanged(int)
{
    ApplyFilters();
}

void AnalysisView::on_comments_only_stateChanged(int)
{
    ApplyFilters();
}

void AnalysisView::on_colorize_stateChanged(int state)
{
    ApplyColumnsPalettes(state);
}


void AnalysisView::on_search_textChanged(const QString &text)
{
    ui->tableView->blockSignals(true);
    proxyModel->blockSignals(true);
    model->blockSignals(true);
    ApplyFilters();
    HighlightSearchMatches(text);
    ui->tableView->blockSignals(false);
    proxyModel->blockSignals(false);
    model->blockSignals(false);
    proxyModel->invalidate();
}


void AnalysisView::keyPressEvent(QKeyEvent *event)
{
    if (event->modifiers() == Qt::ControlModifier)
    {
        switch (event->key())
        {
        case Qt::Key_C:  HandleCopy(); return;
        }
    }

    QWidget::keyPressEvent(event);

}
