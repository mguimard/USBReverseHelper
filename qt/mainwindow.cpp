#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "Parser.h"
#include "StringUtils.h"
#include "Preferences.h"

#include <QInputDialog>
#include <QFileDialog>
#include <QVBoxLayout>
#include <QLabel>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QIcon icon(":/res/USBReverseHelper.png");
    setWindowIcon(icon);

    /*---------------------------------------------------------*\
    | File menu                                                 |
    \*---------------------------------------------------------*/
    QMenu*   file                   = new QMenu("File",                      this);

    QAction* open_folder            = new QAction("Open folder... (ctrl+O)", file);
    QAction* open_recent_session    = new QAction("Recent sessions",         file);
    QAction* exit                   = new QAction("Exit",                    file);

    connect(open_folder,            &QAction::triggered, this, &MainWindow::OpenFolder);
    connect(open_recent_session,    &QAction::triggered, this, &MainWindow::OpenRecentSession);
    connect(exit,                   &QAction::triggered, this, &MainWindow::close);

    file->addAction(open_folder);
    file->addAction(open_recent_session);
    file->addAction(exit);

    menuBar()->addMenu(file);

    /*---------------------------------------------------------*\
    | Tools menu                                                |
    \*---------------------------------------------------------*/
    QMenu* session_menu = new QMenu("Session", this);

    QAction* show_hide_protocol_doc = new QAction("Show/hide protocol doc (F3)", session_menu);
    QAction* show_hide_file_tree = new QAction("Show/hide file tree (F4)", session_menu);
    QAction* reveal_folder = new QAction("Open folder in file browser (F6)", session_menu);

    connect(show_hide_protocol_doc, &QAction::triggered, this, &MainWindow::ToggleProtocolEditor);
    connect(show_hide_file_tree, &QAction::triggered, this, &MainWindow::ToggleFileTree);
    connect(reveal_folder, &QAction::triggered, this, &MainWindow::RevealSessionFolder);

    session_menu->addAction(show_hide_protocol_doc);
    session_menu->addAction(show_hide_file_tree);
    session_menu->addAction(reveal_folder);

    menuBar()->addMenu(session_menu);

    /*---------------------------------------------------------*\
    | About menu                                                |
    \*---------------------------------------------------------*/
    QMenu* help_menu = new QMenu("Help", this);

    QAction* about = new QAction("About", help_menu);
    QAction* help  = new QAction("Help (F1)",  help_menu);

    help_menu->addAction(help);
    help_menu->addAction(about);

    connect(about, &QAction::triggered, this, &MainWindow::ShowAbout);
    connect(help, &QAction::triggered, this, &MainWindow::ShowHelp);

    menuBar()->addMenu(help_menu);

    /*---------------------------------------------------------*\
    | Main content                                              |
    \*---------------------------------------------------------*/
    ui->centralwidget->setLayout(new QVBoxLayout);

    /*---------------------------------------------------------*\
    | Load latest session on startup                            |
    \*---------------------------------------------------------*/
    QString     current_session_path = LatestSessionPath();
    QFile       current_session_file(current_session_path);


    if(current_session_file.exists())
    {
        try
        {
            Session* s = Preferences::Load(current_session_path.toStdString());
            s->SetDocumentation(Preferences::LoadDocumentation(s->GetRootDir()));
            current_session_widget = new SessionWidget(this, s);
        }
        catch(const std::exception& e)
        {
            printf("Cannot load latest session: %s\n", e.what());
            current_session_widget = new SessionWidget(this, "");
        }
    }
    else
    {
        current_session_widget = new SessionWidget(this, "");
    }

    ui->centralwidget->layout()->addWidget(current_session_widget);

    UpdateTitle();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::OpenFolder()
{
    QString dir = QFileDialog::getExistingDirectory(
                this,
                tr("Open Directory"),
                "/home",
                QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    if(!dir.isEmpty())
    {
        OpenSession(dir);
    }
}

void MainWindow::OpenSession(QString dir)
{
    /*---------------------------------------------------------*\
    | Delete from memory the current session                    |
    | Recreate it and add it to the recent sessions list        |
    \*---------------------------------------------------------*/
    delete current_session_widget;

    current_session_widget = new SessionWidget(this, dir);
    ui->centralwidget->layout()->addWidget(current_session_widget);

    Preferences::AddToRecentSessions((QDir::currentPath() + QDir::separator()).toStdString(), dir.toStdString());

    UpdateTitle();
}

void MainWindow::OpenRecentSession()
{
    try
    {
        std::set<std::string> recent_sessions =
                Preferences::GetRecentSessions((QDir::currentPath() + QDir::separator()).toStdString());

        QStringList file_list;

        for(std::string recent_session : recent_sessions)
        {
            file_list << QString::fromStdString(recent_session.c_str());
        }

        if(file_list.empty())
        {
            return;
        }

        QInputDialog inp(this);;

        inp.setOptions(QInputDialog::UseListViewForComboBoxItems);
        inp.setComboBoxItems(file_list);
        inp.setWindowTitle("Load session...");
        inp.setLabelText("Choose session to load:");
        inp.setFixedSize(800, 400);

        if(!inp.exec()){
            return;
        }

        QString session_to_load = inp.textValue();
        OpenSession(session_to_load);

    }
    catch(const std::exception& e)
    {
        printf("Cannot read recent session files");
    }
}

void MainWindow::closeEvent (QCloseEvent *event)
{
    /*---------------------------------------------------------*\
    | Automatically save current session on close event         |
    \*---------------------------------------------------------*/
    SaveCurrentSession();
    event->accept();
}

void MainWindow::SaveCurrentSession()
{
    Preferences::Save(LatestSessionPath().toStdString(), current_session_widget->GetSession());
    Preferences::SaveDocumentation(current_session_widget->GetSession()->GetRootDir(), current_session_widget->GetSession()->GetDocumentation());
}

QString MainWindow::LatestSessionPath()
{
    return  QDir::currentPath() + QDir::separator() + "current_session.json";
}

void MainWindow::ToggleProtocolEditor()
{
    current_session_widget->ToggleEditor();
}

void MainWindow::ToggleFileTree()
{
    current_session_widget->ToggleFileTree();
}

void MainWindow::RevealSessionFolder()
{
    current_session_widget->RevealFolder();
}

void MainWindow::UpdateTitle()
{
    std::string root_dir = current_session_widget->GetSession()->GetRootDir();

    setWindowTitle(QString::fromStdString("USB Reverse Helper" +
                                          (!root_dir.empty() ? " - " + root_dir : "" )));
}

void MainWindow::ShowAbout()
{
    ShowPage("about.html");
}

void MainWindow::ShowHelp()
{
    ShowPage("help.html");
}

void MainWindow::ShowPage(QString page)
{
    QString data;
    QString fileName(":/res/" + page);

    QFile file(fileName);

    if(file.open(QIODevice::ReadOnly))
    {
        data = file.readAll();
    }

    file.close();

    QDialog dialog(this);

    QLabel label(data, &dialog);

    label.setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::TextBrowserInteraction);
    label.setOpenExternalLinks(true);

    dialog.setLayout(new QVBoxLayout(&dialog));
    dialog.layout()->addWidget(&label);
    dialog.exec();
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if (event->modifiers() == Qt::ControlModifier)
    {
        switch (event->key())
        {
            case Qt::Key_S:  SaveCurrentSession(); break;
            case Qt::Key_O:  OpenFolder();         break;
        }
    }
    else
    {
        switch (event->key())
        {
            case Qt::Key_F1: ShowHelp();                break;
            case Qt::Key_F3: ToggleProtocolEditor();    break;
            case Qt::Key_F4: ToggleFileTree();          break;
            case Qt::Key_F6: RevealSessionFolder();     break;

            default: break;
        }
    }
}
