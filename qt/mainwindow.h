#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include "sessionwidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void OpenFolder();
    void OpenRecentSession();
    void ShowAbout();
    void ShowHelp();
    void ToggleProtocolEditor();
    void ToggleFileTree();
    void RevealSessionFolder();

protected:
    void closeEvent(QCloseEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;

private:
    Ui::MainWindow *ui;
    SessionWidget* current_session_widget;

    QString LatestSessionPath();
    void OpenSession(QString dir);
    void SaveCurrentSession();
    void UpdateTitle();
    void ShowPage(QString page);

};
#endif // MAINWINDOW_H
