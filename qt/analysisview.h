#ifndef ANALYSISVIEW_H
#define ANALYSISVIEW_H

#include <QStandardItemModel>
#include <QWidget>
#include <QThread>
#include <QKeyEvent>

#include "analysis.h"
#include "Parser.h"
#include "PacketsSortFilterProxyModel.h"

#define FRAME_COL_INDEX         0
#define TIME_COL_INDEX          1
#define SIZE_COL_INDEX          2
#define TYPE_COL_INDEX          3
#define COMMENTS_COL_INDEX      4
#define VERTICAL_HEADER_COUNT   5

namespace Ui {
class AnalysisView;
}

class ParserWorker : public QObject
{
    Q_OBJECT

public :
    ParserWorker(const std::string &filename): filename(filename){};

private:
    std::string filename;

public slots:
    void run()
    {
        emit ResultReady(Parser::Read(filename));
    }

signals:
    void ResultReady(std::vector<ParsedPacket> packets);
};

class AnalysisView : public QWidget
{
    Q_OBJECT

public:
    explicit AnalysisView(QWidget *parent = nullptr, QString fileName = "");
    explicit AnalysisView(QWidget *parent = nullptr, Analysis * analysis = nullptr);
    ~AnalysisView();

    Analysis* GetAnalysis();

private slots:
    void on_writes_only_stateChanged(int);
    void on_comments_only_stateChanged(int);
    void on_colorize_stateChanged(int);
    void on_packet_size_filter_stateChanged(int);
    void on_packet_size_valueChanged(int);
    void on_dst_selectionChanged();
    void on_search_textChanged(const QString &text);
    void on_tableView_doubleClicked(const QModelIndex & index);
    void on_display_currentIndexChanged(int);
    void OnParserResult(std::vector<ParsedPacket> result);
    void OnVerticalHeaderDoubleClicked(int);

protected:
    void keyPressEvent(QKeyEvent *event) override;

private:
    Ui::AnalysisView*               ui;
    Analysis*                       analysis;
    std::vector<ParsedPacket>       packets;
    QStandardItemModel*             model;
    PacketsSortFilterProxyModel*    proxyModel;
    QThread                         workerThread;

    void                            Setup();
    void                            CreateTableView();
    void                            UpdatePacketFilters();
    void                            ApplyFilters();
    void                            ApplyColumnsPalettes(bool);
    void                            HighlightSearchMatches(const QString&);
    void                            ShowPacketDetails(int i);
    void                            HandleCopy();

    PacketFilters                   filters;
};

#endif // ANALYSISVIEW_H
