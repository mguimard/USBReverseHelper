#ifndef PACKETDETAILSVIEW_H
#define PACKETDETAILSVIEW_H

#include "ParsedPacket.h"
#include <QWidget>
#include <QStandardItemModel>

namespace Ui {
class PacketDetailsView;
}

class PacketDetailsView : public QWidget
{
    Q_OBJECT

public:
    explicit PacketDetailsView(ParsedPacket packet, QWidget *parent = nullptr);
    ~PacketDetailsView();

private:
    Ui::PacketDetailsView *ui;

    QStandardItemModel* hexa_model;
    QStandardItemModel* text_model;
};

#endif // PACKETDETAILSVIEW_H
