#ifndef PACKETSSORTFILTERPROXYMODEL_H
#define PACKETSSORTFILTERPROXYMODEL_H

#include <QSortFilterProxyModel>
#include <QObject>
#include "ParsedPacket.h"
#include <regex>

class PacketsSortFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT

public:

    PacketsSortFilterProxyModel(std::vector<ParsedPacket> packets)
        : packets(packets){

        connect(this, SIGNAL(Filtered(int,bool)), this, SLOT(OnFiltered(int,bool)));
    };

    PacketFilters filters;

    bool Filter(const ParsedPacket& packet) const
    {
        /*---------------------------------------------------------*\
        | Writes only filter                                        |
        \*---------------------------------------------------------*/
        if(filters.writes_only && !packet.isWrite())
        {
            return false;
        }

        /*---------------------------------------------------------*\
        | Comments only filter                                      |
        \*---------------------------------------------------------*/
        if(filters.comments_only && packet.comment.empty())
        {
            return false;
        }

        /*---------------------------------------------------------*\
        | Packet size filter                                        |
        \*---------------------------------------------------------*/
        if(filters.size_filter && packet.data_len != filters.size)
        {
            return false;
        }

        /*---------------------------------------------------------*\
        | dst device filter                                         |
        \*---------------------------------------------------------*/
        if(!filters.selected_dst_devices.empty() && filters.selected_dst_devices.find(packet.dst) == filters.selected_dst_devices.end())
        {
            return false;
        }

        /*---------------------------------------------------------*\
        | Content filter                                            |
        \*---------------------------------------------------------*/
        try
        {
            if(!filters.search.empty() && !std::regex_search(StringUtils::bytes_to_string(packet.data), std::regex(filters.search, std::regex_constants::icase)))
            {
                return false;
            }
        }
        catch (const std::exception& e)
        {
            printf("Cannot search with regex\"%s\": %s", filters.search.c_str(), e.what());
        }

        return true;
    }

    std::vector<ParsedPacket> GetFilteredPackets();

signals:
    void Filtered(int,bool) const;

private slots:
    void OnFiltered(int i, bool s)
    {
        filtered[i] = s;
    };

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &/*sourceParent*/) const override
    {
        bool res = Filter(packets[sourceRow]);
        emit Filtered(sourceRow, res);
        return res;
    };
private:
    std::vector<ParsedPacket>  packets;
    std::map<int,bool>         filtered;
};

inline std::vector<ParsedPacket> PacketsSortFilterProxyModel::GetFilteredPackets()
{
    std::vector<ParsedPacket> filtered_packets;

    for(ParsedPacket& packet: packets)
    {
        if(filtered[packet.frame - 1])
        {
            filtered_packets.push_back(packet);
        }
    }

    return filtered_packets;
}


#endif // PACKETSSORTFILTERPROXYMODEL_H
