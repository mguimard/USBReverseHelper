#ifndef SESSIONWIDGET_H
#define SESSIONWIDGET_H

#include <QWidget>
#include <QFileSystemModel>

#include "session.h"

namespace Ui {
class SessionWidget;
}

class SessionWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SessionWidget(QWidget *parent = nullptr, QString dir = "");
    explicit SessionWidget(QWidget *parent = nullptr, Session* = nullptr);

    ~SessionWidget();
    Session* GetSession();

    void ToggleEditor();
    void ToggleFileTree();
    void RevealFolder();

private slots:
    void OnFileTreeDoubleClicked(QModelIndex index);
    void OnAnalysisCloseRequest(int index);
    void on_editor_textChanged();

private:
    Ui::SessionWidget *ui;
    Session* session;
    QFileSystemModel *file_system_model;
    void Init();
};

#endif // SESSIONWIDGET_H
