#include "packetdetailsview.h"
#include "ui_packetdetailsview.h"


PacketDetailsView::PacketDetailsView(ParsedPacket packet, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PacketDetailsView)
{
    ui->setupUi(this);

    if(packet.comment.empty())
    {
        ui->comment_frame->hide();
    }
    else
    {
        ui->packet_comment->setText(QString::fromStdString(packet.comment));
    }

    QString summary = QString::fromStdString("Frame #" + std::to_string(packet.frame));
    ui->summary->setText(summary);

    int cols = 16;
    int rows = packet.data_len / 16 + 1;

    hexa_model = new QStandardItemModel(rows, cols , this);
    text_model = new QStandardItemModel(rows, cols , this);

    for(int row = 0; row < rows; row++)
    {
        for(int col = 0; col < cols; col++)
        {
            unsigned int i = row * 16 + col;
            if( i < packet.data_len)
            {
                hexa_model->setData(hexa_model->index(row, col, QModelIndex()),
                    QString::fromStdString(StringUtils::hexa_string(packet.data[i])));

                text_model->setData(text_model->index(row, col, QModelIndex()),
                    QString::fromStdString(StringUtils::ascii_string(packet.data[i])));
            }
        }        

        hexa_model->setHeaderData(row, Qt::Vertical, QString::fromStdString(StringUtils::hexa_string_16(row * 16)));
        text_model->setHeaderData(row, Qt::Vertical, QString::fromStdString(StringUtils::hexa_string_16(row * 16)));
    }

    ui->hexa->setModel(hexa_model);
    ui->text->setModel(text_model);

    ui->hexa->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    ui->text->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);

    for(int col = 0; col < cols; col++)
    {
        ui->hexa->setColumnWidth(col, 40);
        ui->text->setColumnWidth(col, 40);

        hexa_model->setHeaderData(col, Qt::Horizontal, QString::fromStdString(StringUtils::hexa_string(col)));
        text_model->setHeaderData(col, Qt::Horizontal, QString::fromStdString(StringUtils::hexa_string(col)));
    }

    //connect(ui->hexa->selectionModel(), SIGNAL(currentChanged(const QModelIndex &,const QModelIndex &)), ui->text, SLOT(setCurrentIndex(const QModelIndex &)));
    //connect(ui->text->selectionModel(), SIGNAL(currentChanged(const QModelIndex &,const QModelIndex &)), ui->hexa, SLOT(setCurrentIndex(const QModelIndex &)));
}

PacketDetailsView::~PacketDetailsView()
{
    delete ui;
}
