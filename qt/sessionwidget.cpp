#include "analysisview.h"
#include "sessionwidget.h"
#include "ui_sessionwidget.h"

#include "session.h"
#include "analysis.h"

#include <QLabel>
#include <QToolButton>
#include <QTabBar>
#include <QFileDialog>
#include <QDir>
#include <QDesktopServices>
#include <QUrl>

SessionWidget::SessionWidget(QWidget *parent, QString dir) :
    QWidget(parent),
    ui(new Ui::SessionWidget)
{    
    session = new Session(dir.toStdString());
    Init();
}

SessionWidget::SessionWidget(QWidget *parent, Session* session) :
    QWidget(parent),
    ui(new Ui::SessionWidget),
    session(session)
{
    Init();
}

void SessionWidget::ToggleEditor()
{
    ui->editor->setVisible(!ui->editor->isVisible());
}

void SessionWidget::ToggleFileTree()
{
    ui->file_tree->setVisible(!ui->file_tree->isVisible());
}

void SessionWidget::RevealFolder()
{
    QDesktopServices::openUrl(QUrl::fromLocalFile(QString::fromStdString(session->GetRootDir())));
}

void SessionWidget::Init()
{
    ui->setupUi(this);

    ui->editor->hide();
    ui->editor->setLineWrapMode(QPlainTextEdit::LineWrapMode::NoWrap);
    ui->editor->setPlainText(QString::fromStdString(session->GetDocumentation()));

    QString path = QString::fromStdString(session->GetRootDir());

    /*---------------------------------------------------------*\
    | Initialize tree view                                      |
    \*---------------------------------------------------------*/
    file_system_model = new QFileSystemModel(this);

    QString root_dir(QString::fromStdString(session->GetRootDir()));

    file_system_model->setRootPath(root_dir);
    file_system_model->setReadOnly(true);

    ui->file_tree->setModel(file_system_model);
    ui->file_tree->setRootIndex(file_system_model->setRootPath(root_dir));

    file_system_model->setNameFilterDisables(false);
    file_system_model->setNameFilters({"*.pcapng"});

    ui->file_tree->hideColumn(1);
    ui->file_tree->hideColumn(2);
    ui->file_tree->hideColumn(3);

    connect(ui->file_tree, &QTreeView::doubleClicked, this, &SessionWidget::OnFileTreeDoubleClicked);

    /*---------------------------------------------------------*\
    | Initialize tabs                                           |
    \*---------------------------------------------------------*/
    ui->tabs->clear();
    ui->tabs->setTabsClosable(true);

    connect(ui->tabs, &QTabWidget::tabCloseRequested, this, &SessionWidget::OnAnalysisCloseRequest);

    /*---------------------------------------------------------*\
    | Session opened analyses                                   |
    \*---------------------------------------------------------*/
    for(Analysis* analysis: session->GetAnalyses())
    {
        if(analysis->HasFiles())
        {
            std::string file = analysis->GetFiles().front();
            QString path(QString::fromStdString(file));
            QFileInfo fi(path);
            AnalysisView* view = new AnalysisView(this, analysis);
            ui->tabs->addTab(view, fi.fileName());
        }
    }
}

SessionWidget::~SessionWidget()
{
    delete ui;
}

Session* SessionWidget::GetSession()
{
    return session;
}

void SessionWidget::OnFileTreeDoubleClicked(QModelIndex index)
{
    /*---------------------------------------------------------*\
    |Do not attempt to parse a folder                           |
    \*---------------------------------------------------------*/
    if(file_system_model->isDir(index))
    {
        return;
    }

    QString root_dir(QString::fromStdString(session->GetRootDir()));
    QString path = file_system_model->filePath(index);

    if(path.endsWith(".pcapng"))
    {
        AnalysisView* view = new AnalysisView(this, path);
        QFileInfo fi(path);
        int new_tab_index = ui->tabs->addTab(view, fi.fileName());
        ui->tabs->setCurrentIndex(new_tab_index);
        session->AddAnalysis(view->GetAnalysis());
    }
}

void SessionWidget::OnAnalysisCloseRequest(int index)
{
    session->RemoveAnalysis(((AnalysisView*)ui->tabs->widget(index))->GetAnalysis());
    delete ui->tabs->widget(index);
}

void SessionWidget::on_editor_textChanged()
{
    session->SetDocumentation(ui->editor->toPlainText().toStdString());
}


