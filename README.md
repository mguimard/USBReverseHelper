# USB Reverse Helper

[![pipeline status](https://gitlab.com/mguimard/USBReverseHelper/badges/master/pipeline.svg)](https://gitlab.com/mguimard/USBReverseHelper/-/commits/master)

## What is this?

This is a tool that aims to help analyzing pcapng files.

## Downloads

* [Windows 64](https://gitlab.com/mguimard/USBReverseHelper/-/jobs/artifacts/master/download?job=Windows%2064)
* [Linux 64](https://gitlab.com/mguimard/USBReverseHelper/-/jobs/artifacts/master/download?job=Linux%2064%20AppImage)

## How do I install it?

* Download and extract the correct files depending on your system
* Launch the executable file

### Linux

You need `libpcap` installed.

## Contributing

Please read the [contributing guide](./CONTRIBUTING.md) if you want to want to fix bugs and/or add new features.

