#ifndef PARSER_H
#define PARSER_H

#include "ColorUtils.h"
#include "Packet.h"
#include "ParsedPacket.h"
#include "PcapFileDevice.h"
#include "StringUtils.h"
#include <string>
#include <map>
#include <set>

using namespace pcpp;

class Parser
{
public:

    /*---------------------------------------------------------*\
    | Read a pcapng file                                        |
    \*---------------------------------------------------------*/
    static std::vector<ParsedPacket> Read(std::string inputFileName)
    {
        std::vector<ParsedPacket> packets;

        PcapNgFileReaderDevice readerDev(inputFileName);

        readerDev.open();

        RawPacket rawPacket;

        unsigned int frame = 0;

        std::string comment;

        while (readerDev.getNextPacket(rawPacket, comment))
        {
            ParsedPacket parsed_packet;
            unsigned int data_len;
            const uint8_t* raw_data         = rawPacket.getRawData();
            const unsigned int raw_data_len = rawPacket.getRawDataLen();

            parsed_packet.ts = rawPacket.getPacketTimeStamp();
            parsed_packet.comment = comment;

            /*---------------------------------------------------------*\
            | Pseudo header stuff                                       |
            \*---------------------------------------------------------*/
            parsed_packet.frame             = ++frame;
            parsed_packet.urb_function      = raw_data[0x0E] + (raw_data[0x0F] << 8);

            if(parsed_packet.urb_function == NOT_RELEVANT)
            {
                printf("IGNORING frame %d\n", parsed_packet.frame);
                continue;
            }

            /*---------------------------------------------------------*\
            | Parsing different packet types                            |
            \*---------------------------------------------------------*/
            switch(parsed_packet.urb_function)
            {
            /*---------------------------------------------------------*\
            | GET DESCRIPTOR                                            |
            \*---------------------------------------------------------*/
            case URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE:
                parsed_packet.irp_direction     = raw_data[0x10] & 0x0F;
                parsed_packet.dst               =
                        std::to_string(raw_data[0x11]) + "." +
                        std::to_string(raw_data[0x13]) + "." +
                        std::to_string(raw_data[0x15] & 0x0F);

                parsed_packet.direction         = raw_data[0x15] >> 7;
                parsed_packet.endpoint          = raw_data[0x15] & 0x0F;
                parsed_packet.urb_transfer_type = raw_data[0x16];

                data_len =
                        (raw_data[0x17]) +
                        (raw_data[0x18] << 8 ) +
                        (raw_data[0x19] << 16) +
                        (raw_data[0x1A] << 24) ;

                if(parsed_packet.irp_direction == FDO_PDO)
                {
                    parsed_packet.b_descriptor_type = raw_data[0x1F];
                }
                else
                {
                    parsed_packet.b_descriptor_type = raw_data[0x1D];
                }

                break;

            /*---------------------------------------------------------*\
            | SET CONFIGURATION                                         |
            \*---------------------------------------------------------*/
            case URB_FUNCTION_SELECT_CONFIGURATION:
                parsed_packet.irp_direction     = raw_data[0x10] & 0x0F;

                parsed_packet.dst               =
                        std::to_string(raw_data[0x11]) + "." +
                        std::to_string(raw_data[0x13]) + "." +
                        std::to_string(raw_data[0x15] & 0x0F);

                parsed_packet.direction         = raw_data[0x15] >> 7;
                parsed_packet.endpoint          = raw_data[0x15] & 0x0F;

                data_len =
                        (raw_data[0x17]) +
                        (raw_data[0x18] << 8 ) +
                        (raw_data[0x19] << 16) +
                        (raw_data[0x1A] << 24) ;

                if(parsed_packet.irp_direction == FDO_PDO)
                {
                    parsed_packet.b_request = raw_data[0x1D];
                }

                break;

            /*---------------------------------------------------------*\
            | URB_INTERRUPT                                             |
            \*---------------------------------------------------------*/
            case URB_FUNCTION_BULK_OR_INTERRUPT_TRANSFER:

                parsed_packet.irp_direction     = raw_data[0x10] & 0x0F;

                parsed_packet.dst               =
                        std::to_string(raw_data[0x11]) + "." +
                        std::to_string(raw_data[0x13]) + "." +
                        std::to_string(raw_data[0x15] & 0x0F);

                parsed_packet.direction         = raw_data[0x15] >> 7;
                parsed_packet.endpoint          = raw_data[0x15] & 0x0F;

                data_len =
                        (raw_data[0x17]) +
                        (raw_data[0x18] << 8 ) +
                        (raw_data[0x19] << 16) +
                        (raw_data[0x1A] << 24) ;

                break;

            /*---------------------------------------------------------*\
            | GET DESCRIPTOR Response device / SET REPORT Response      |
            \*---------------------------------------------------------*/
            case URB_FUNCTION_CONTROL_TRANSFER:
            case URB_FUNCTION_CONTROL_TRANSFER_EX:

                parsed_packet.irp_direction     = raw_data[0x10] & 0x0F;
                parsed_packet.dst               =
                        std::to_string(raw_data[0x11]) + "." +
                        std::to_string(raw_data[0x13]) + "." +
                        std::to_string(raw_data[0x15] & 0x0F);

                parsed_packet.direction         = raw_data[0x15] >> 7;
                parsed_packet.endpoint          = raw_data[0x15] & 0x0F;
                parsed_packet.urb_transfer_type = raw_data[0x16];

                if(raw_data[0x1F] == REPORT_TYPE_OUTPUT && parsed_packet.irp_direction == FDO_PDO)
                {
                    data_len =
                        (raw_data[0x22]) +
                        (raw_data[0x23] << 8 );
                }else
                {
                    data_len =
                        (raw_data[0x17]) +
                        (raw_data[0x18] << 8 ) +
                        (raw_data[0x19] << 16) +
                        (raw_data[0x1A] << 24) ;
                }

                break;

            /*---------------------------------------------------------*\
            | SET REPORT Request / URB CONTROL                          |
            \*---------------------------------------------------------*/
            case URB_FUNCTION_CLASS_INTERFACE:
            {
                parsed_packet.urb_transfer_type = raw_data[0x16];

                parsed_packet.irp_direction     = raw_data[0x10] & 0x0F;

                parsed_packet.dst               =
                        std::to_string(raw_data[0x11]) + "." +
                        std::to_string(raw_data[0x13]) + "." +
                        std::to_string(raw_data[0x15] & 0x0F);

                parsed_packet.direction         = raw_data[0x15] >> 7;
                parsed_packet.endpoint          = raw_data[0x15] & 0x0F;                

                if(parsed_packet.direction == DIRECTION_IN)
                {
                    data_len = 0;
                }
                else
                {
                    parsed_packet.b_request = raw_data[0x1D];

                    int packet_data_len =
                            (raw_data[0x17]) +
                            (raw_data[0x18] << 8 ) +
                            (raw_data[0x19] << 16) +
                            (raw_data[0x1A] << 24) ;

                    if(packet_data_len > 0)
                    {
                        int setup_data_offset = raw_data_len - packet_data_len;

                        data_len =
                                (raw_data[setup_data_offset + 0x06]) +
                                (raw_data[setup_data_offset + 0x07] << 8 );
                    }
                    else
                    {
                        data_len = packet_data_len;
                    }
                }

                break;
            }

            /*---------------------------------------------------------*\
            | GET DESCRIPTOR Request HID Report                         |
            \*---------------------------------------------------------*/
            case URB_FUNCTION_GET_DESCRIPTOR_FROM_INTERFACE:

                parsed_packet.irp_direction     = raw_data[0x10] & 0x0F;

                parsed_packet.dst               =
                        std::to_string(raw_data[0x11]) + "." +
                        std::to_string(raw_data[0x13]) + "." +
                        std::to_string(raw_data[0x15] & 0x0F);

                parsed_packet.direction         = raw_data[0x15] >> 7;
                parsed_packet.endpoint          = raw_data[0x15] & 0x0F;

                data_len =
                        (raw_data[0x17]) +
                        (raw_data[0x18] << 8 ) +
                        (raw_data[0x19] << 16) +
                        (raw_data[0x1A] << 24) ;

                break;

            /*---------------------------------------------------------*\
            | URB_ISOCHRONOUS                                           |
            \*---------------------------------------------------------*/
            case URB_FUNCTION_ISOCH_TRANSFER:

                parsed_packet.irp_direction     = raw_data[0x10] & 0x0F;

                parsed_packet.dst               =
                        std::to_string(raw_data[0x11]) + "." +
                        std::to_string(raw_data[0x13]) + "." +
                        std::to_string(raw_data[0x15] & 0x0F);

                parsed_packet.direction         = raw_data[0x15] >> 7;
                parsed_packet.endpoint          = raw_data[0x15] & 0x0F;

                data_len =
                        (raw_data[0x17]) +
                        (raw_data[0x18] << 8 ) +
                        (raw_data[0x19] << 16) +
                        (raw_data[0x1A] << 24) ;

                break;

            /*---------------------------------------------------------*\
            | Ignore data on unknown types                              |
            \*---------------------------------------------------------*/
            default:  data_len = 0; break;

            }

            /*---------------------------------------------------------*\
            | Data fragment                                             |
            \*---------------------------------------------------------*/
            unsigned int data_start = raw_data_len - data_len;

            for(unsigned int i = data_start; i < raw_data_len; i++)
            {
                parsed_packet.data.push_back(raw_data[i]);
            }

            parsed_packet.data_len = data_len;

            packets.push_back(parsed_packet);
        }

        readerDev.close();

        return packets;
    }

};

#endif // PARSER_H
