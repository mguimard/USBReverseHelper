#ifndef PARSEDPACKET_H
#define PARSEDPACKET_H

#include <cstdint>
#include <string>
#include <set>
#include <vector>
#include "StringUtils.h"

#ifdef _MSC_VER
#include <WinSock2.h>
#include <time.h>
#else
#include <sys/time.h>
#endif

typedef struct PacketFilters
{
    bool                        writes_only;
    bool                        comments_only;
    bool                        size_filter;
    unsigned int                size;
    std::string                 search;
    std::set<std::string>       selected_dst_devices;
} PacketFilters;

/*---------------------------------------------------------*\
| URB functions                                             |
\*---------------------------------------------------------*/
enum
{
    URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE         = 0x000B,
    URB_FUNCTION_CONTROL_TRANSFER                   = 0x0008,
    URB_FUNCTION_SELECT_CONFIGURATION               = 0x0000,
    URB_FUNCTION_CLASS_INTERFACE                    = 0x001B,
    URB_FUNCTION_BULK_OR_INTERRUPT_TRANSFER         = 0x0009,
    URB_FUNCTION_GET_MS_FEATURE_DESCRIPTOR          = 0x002A,
    URB_FUNCTION_GET_DESCRIPTOR_FROM_INTERFACE      = 0x0028,
    URB_FUNCTION_CONTROL_TRANSFER_EX                = 0x0032,
    URB_FUNCTION_ABORT_PIPE                         = 0x0002,
    URB_FUNCTION_ISOCH_TRANSFER                     = 0x000A,
    NOT_RELEVANT                                    = 0x002D
};

/*---------------------------------------------------------*\
| URB type                                                  |
\*---------------------------------------------------------*/
enum
{
    URB_COMPLETE                                      = 0x43,
    URB_SUBMIT                                        = 0x53
};

/*---------------------------------------------------------*\
| Report type                                                |
\*---------------------------------------------------------*/
enum
{
    REPORT_TYPE_OUTPUT                                = 0x02,
    REPORT_TYPE_FEATURE                               = 0x03
};

/*---------------------------------------------------------*\
| URB transfer type                                         |
\*---------------------------------------------------------*/
enum
{
    URB_ISOCHRONOUS                                   = 0x00,
    URB_INTERRUPT                                     = 0x01,
    URB_CONTROL                                       = 0x02,
};

/*---------------------------------------------------------*\
| URB direction                                             |
\*---------------------------------------------------------*/
enum
{
    DIRECTION_IN                                      = 0x01,
    DIRECTION_OUT                                     = 0x00
};

/*---------------------------------------------------------*\
| b descriptor type                                         |
\*---------------------------------------------------------*/
enum
{
    DEVICE                                            = 0x01,
    CONFIGURATION                                     = 0x02
};
/*---------------------------------------------------------*\
| b request                                                 |
\*---------------------------------------------------------*/
enum
{
    SET_CONFIGURATION                                 = 0x09,
    SET_IDLE                                          = 0x0A
};

/*---------------------------------------------------------*\
| IRP direction                                             |
| PDO = Physical Device Object                              |
| FDO = Functional Device Object                            |
\*---------------------------------------------------------*/
enum
{
    FDO_PDO                                           = 0x00, // to device
    PDO_FDO                                           = 0x01  // to host
};

typedef struct ParsedPacket{

    unsigned int            frame;
    timespec                ts;
    uint8_t                 urb_function;
    uint8_t                 b_descriptor_type;
    uint8_t                 b_request;
    uint8_t                 urb_type;
    uint8_t                 urb_transfer_type;
    uint8_t                 endpoint;
    uint8_t                 direction;
    uint8_t                 device;
    uint8_t                 irp_direction;
    uint8_t                 report_type;
    unsigned int            data_len;
    uint8_t                 urb_bus_id;
    std::vector<uint8_t>    data;
    std::string             dst;
    std::string             comment;

    /*---------------------------------------------------------*\
    | not very accurate, but better than nothing                |
    \*---------------------------------------------------------*/
    std::string get_urb_function_name() const
    {
        std::string ret;

        switch (this->urb_function)
        {

        case URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE:
            ret = "GET DESCRIPTOR Request";

            if(this->b_descriptor_type == DEVICE)
            {
                ret.append(" DEVICE");
            }
            else if(this->b_descriptor_type == CONFIGURATION)
            {
                ret.append(" CONFIGURATION");
            }
            break;

        case URB_FUNCTION_SELECT_CONFIGURATION:

            ret = "SET CONFIGURATION";

            if(this->irp_direction == FDO_PDO)
            {
                ret.append(" Request");
            }
            else if(this->b_descriptor_type == CONFIGURATION)
            {
                ret.append(" Response");
            }
            break;


        case URB_FUNCTION_BULK_OR_INTERRUPT_TRANSFER:
            ret = "URB_INTERRUPT";
            if(this->direction == DIRECTION_OUT)
            {
                ret.append(" out");
            }
            else
            {
                ret.append(" in");
            }
            break;


        case URB_FUNCTION_CONTROL_TRANSFER:
        case URB_FUNCTION_CONTROL_TRANSFER_EX:
            if(this->irp_direction == PDO_FDO)
            {
                if(this->direction == DIRECTION_OUT)
                {
                    ret = "SET_REPORT Response";
                }
                else
                {
                    ret = "GET_REPORT Response";
                }
            }
            else
            {
                if(this->direction == DIRECTION_OUT)
                {
                    ret = "SET_REPORT Request";
                }
                else
                {
                    ret = "URB_FUNCTION_CONTROL_TRANSFER FDO_PDO DIRECTION_IN";
                }
            }

            break;

        case URB_FUNCTION_CLASS_INTERFACE:
            if(this->direction == DIRECTION_OUT)
            {
                if(this->b_request == SET_IDLE)
                {
                    ret = "SET_IDLE Request";
                }
                else
                {
                    ret = "SET_REPORT Request";
                }
            }
            else
            {
                ret = "GET_REPORT Request";
            }
            break;

        case URB_FUNCTION_GET_MS_FEATURE_DESCRIPTOR:
            ret = "URB_FUNCTION_GET_MS_FEATURE_DESCRIPTOR";
            break;

        case URB_FUNCTION_GET_DESCRIPTOR_FROM_INTERFACE:
            ret = "GET DESCRIPTOR Request HID Report";
            break;

        case URB_FUNCTION_ABORT_PIPE:
            ret = "URB_FUNCTION_ABORT_PIPE";
            break;

        case URB_FUNCTION_ISOCH_TRANSFER:
            ret = "URB_ISOCHRONOUS";

            if(this->direction == DIRECTION_OUT)
            {
                ret.append(" out");
            }

            break;

        default:
            ret = "Unknown type "+ StringUtils::hexa_string(this->urb_function);
            break;
        }

        return ret;
    }

    bool isWrite() const
    {
        return this->irp_direction == FDO_PDO && this->direction == DIRECTION_OUT;
    }

} ParsedPacket;

#endif // PARSEDPACKET_H
