QT      += core gui widgets
requires(qtConfig(treeview))
CONFIG  += c++17

#-----------------------------------------------------------------------------------------------#
# App sources                                                                                   #
#-----------------------------------------------------------------------------------------------#
SOURCES +=                                                                                      \
    main.cpp                                                                                    \
    models/analysis.cpp                                                                         \
    models/session.cpp                                                                          \
    qt/analysisview.cpp                                                                         \
    qt/mainwindow.cpp                                                                           \
    qt/packetdetailsview.cpp \
    qt/sessionwidget.cpp                                                                        \

HEADERS +=                                                                                      \
    ColorUtils.h                                                                                \
    ParsedPacket.h                                                                              \
    Parser.h                                                                                    \
    Preferences.h                                                                               \
    StringUtils.h                                                                               \
    models/analysis.h                                                                           \
    models/session.h                                                                            \
    qt/PacketsSortFilterProxyModel.h \
    qt/analysisview.h                                                                           \
    qt/mainwindow.h                                                                             \
    qt/packetdetailsview.h \
    qt/sessionwidget.h                                                                          \

FORMS +=                                                                                        \
    qt/analysisview.ui                                                                          \
    qt/mainwindow.ui                                                                            \
    qt/packetdetailsview.ui \
    qt/sessionwidget.ui                                                                         \

INCLUDEPATH +=                                                                                  \
    models                                                                                      \
    qt                                                                                          \

#-----------------------------------------------------------------------------------------------#
# Qt-MultiSelectComboBox                                                                        #
#-----------------------------------------------------------------------------------------------#
INCLUDEPATH +=                                                                                  \
    dependencies/Qt-MultiSelectComboBox

HEADERS +=                                                                                      \
    dependencies/Qt-MultiSelectComboBox/MultiSelectComboBox.h

SOURCES +=                                                                                      \
    dependencies/Qt-MultiSelectComboBox/MultiSelectComboBox.cpp

#-----------------------------------------------------------------------------------------------#
# json                                                                                          #
#-----------------------------------------------------------------------------------------------#
INCLUDEPATH +=                                                                                  \
    dependencies/json/include

#-----------------------------------------------------------------------------------------------#
# PcapPlusPlus                                                                                  #
#-----------------------------------------------------------------------------------------------#
INCLUDEPATH +=                                                                                  \
    dependencies/PcapPlusPlus/Pcap++/header                                                     \
    dependencies/PcapPlusPlus/Common++/header                                                   \
    dependencies/PcapPlusPlus/Packet++/header                                                   \
    dependencies/PcapPlusPlus/3rdParty/LightPcapNg/LightPcapNg/include                          \
    dependencies/PcapPlusPlus/3rdParty/EndianPortable/include                                   \
    dependencies/PcapPlusPlus/3rdParty/hash-library                                             \

SOURCES +=                                                                                      \
    dependencies/PcapPlusPlus/3rdParty/hash-library/md5.cpp                                     \
    dependencies/PcapPlusPlus/Pcap++/src/PcapFileDevice.cpp                                     \
    dependencies/PcapPlusPlus/Pcap++/src/PcapFilter.cpp                                         \
    dependencies/PcapPlusPlus/Pcap++/src/PcapDevice.cpp                                         \
    dependencies/PcapPlusPlus/Common++/src/Logger.cpp                                           \
    dependencies/PcapPlusPlus/Common++/src/MacAddress.cpp                                       \
    dependencies/PcapPlusPlus/Common++/src/GeneralUtils.cpp                                     \
    dependencies/PcapPlusPlus/Common++/src/IpAddress.cpp                                        \
    dependencies/PcapPlusPlus/Common++/src/SystemUtils.cpp                                      \
    dependencies/PcapPlusPlus/3rdParty/LightPcapNg/LightPcapNg/src/light_advanced.c             \
    dependencies/PcapPlusPlus/3rdParty/LightPcapNg/LightPcapNg/src/light_alloc.c                \
    dependencies/PcapPlusPlus/3rdParty/LightPcapNg/LightPcapNg/src/light_compression.c          \
    dependencies/PcapPlusPlus/3rdParty/LightPcapNg/LightPcapNg/src/light_internal.c             \
    dependencies/PcapPlusPlus/3rdParty/LightPcapNg/LightPcapNg/src/light_io.c                   \
    dependencies/PcapPlusPlus/3rdParty/LightPcapNg/LightPcapNg/src/light_manipulate.c           \
    dependencies/PcapPlusPlus/3rdParty/LightPcapNg/LightPcapNg/src/light_null_compression.c     \
    dependencies/PcapPlusPlus/3rdParty/LightPcapNg/LightPcapNg/src/light_pcapng_cont.c          \
    dependencies/PcapPlusPlus/3rdParty/LightPcapNg/LightPcapNg/src/light_pcapng_ext.c           \
    dependencies/PcapPlusPlus/3rdParty/LightPcapNg/LightPcapNg/src/light_platform.c             \
    dependencies/PcapPlusPlus/3rdParty/LightPcapNg/LightPcapNg/src/light_zstd_compression.c     \
    dependencies/PcapPlusPlus/3rdParty/LightPcapNg/LightPcapNg/src/light_option.c               \
    dependencies/PcapPlusPlus/3rdParty/LightPcapNg/LightPcapNg/src/light_pcapng.c               \
    dependencies/PcapPlusPlus/Packet++/src/RawPacket.cpp                                        \
    dependencies/PcapPlusPlus/Packet++/src/Packet.cpp                                           \
    dependencies/PcapPlusPlus/Packet++/src/Layer.cpp                                            \
    dependencies/PcapPlusPlus/Packet++/src/PacketUtils.cpp                                      \
    dependencies/PcapPlusPlus/Packet++/src/TLVData.cpp                                          \
    dependencies/PcapPlusPlus/Packet++/src/DnsResource.cpp                                      \
    dependencies/PcapPlusPlus/Packet++/src/DnsResourceData.cpp                                  \
    dependencies/PcapPlusPlus/Packet++/src/IPSecLayer.cpp                                       \
    dependencies/PcapPlusPlus/Packet++/src/SSHLayer.cpp                                         \
    dependencies/PcapPlusPlus/Packet++/src/EthLayer.cpp                                         \
    dependencies/PcapPlusPlus/Packet++/src/PacketTrailerLayer.cpp                               \
    dependencies/PcapPlusPlus/Packet++/src/GreLayer.cpp                                         \
    dependencies/PcapPlusPlus/Packet++/src/DnsLayer.cpp                                         \
    dependencies/PcapPlusPlus/Packet++/src/DhcpLayer.cpp                                        \
    dependencies/PcapPlusPlus/Packet++/src/IPv6Layer.cpp                                        \
    dependencies/PcapPlusPlus/Packet++/src/IcmpV6Layer.cpp                                      \
    dependencies/PcapPlusPlus/Packet++/src/PayloadLayer.cpp                                     \
    dependencies/PcapPlusPlus/Packet++/src/VlanLayer.cpp                                        \
    dependencies/PcapPlusPlus/Packet++/src/IPv4Layer.cpp                                        \
    dependencies/PcapPlusPlus/Packet++/src/SipLayer.cpp                                         \
    dependencies/PcapPlusPlus/Packet++/src/HttpLayer.cpp                                        \
    dependencies/PcapPlusPlus/Packet++/src/NtpLayer.cpp                                         \
    dependencies/PcapPlusPlus/Packet++/src/NdpLayer.cpp                                         \
    dependencies/PcapPlusPlus/Packet++/src/LLCLayer.cpp                                         \
    dependencies/PcapPlusPlus/Packet++/src/StpLayer.cpp                                         \
    dependencies/PcapPlusPlus/Packet++/src/PPPoELayer.cpp                                       \
    dependencies/PcapPlusPlus/Packet++/src/UdpLayer.cpp                                         \
    dependencies/PcapPlusPlus/Packet++/src/MplsLayer.cpp                                        \
    dependencies/PcapPlusPlus/Packet++/src/SdpLayer.cpp                                         \
    dependencies/PcapPlusPlus/Packet++/src/TcpLayer.cpp                                         \
    dependencies/PcapPlusPlus/Packet++/src/BgpLayer.cpp                                         \
    dependencies/PcapPlusPlus/Packet++/src/SomeIpLayer.cpp                                      \
    dependencies/PcapPlusPlus/Packet++/src/IcmpLayer.cpp                                        \
    dependencies/PcapPlusPlus/Packet++/src/IgmpLayer.cpp                                        \
    dependencies/PcapPlusPlus/Packet++/src/NullLoopbackLayer.cpp                                \
    dependencies/PcapPlusPlus/Packet++/src/SllLayer.cpp                                         \
    dependencies/PcapPlusPlus/Packet++/src/VxlanLayer.cpp                                       \
    dependencies/PcapPlusPlus/Packet++/src/ArpLayer.cpp                                         \
    dependencies/PcapPlusPlus/Packet++/src/EthDot3Layer.cpp                                     \
    dependencies/PcapPlusPlus/Packet++/src/RadiusLayer.cpp                                      \
    dependencies/PcapPlusPlus/Packet++/src/GtpLayer.cpp                                         \
    dependencies/PcapPlusPlus/Packet++/src/DhcpV6Layer.cpp                                      \
    dependencies/PcapPlusPlus/Packet++/src/SSLHandshake.cpp                                     \
    dependencies/PcapPlusPlus/Packet++/src/SSLLayer.cpp                                         \
    dependencies/PcapPlusPlus/Packet++/src/SSLCommon.cpp                                        \
    dependencies/PcapPlusPlus/Packet++/src/IPv6Extensions.cpp                                   \
    dependencies/PcapPlusPlus/Packet++/src/TextBasedProtocol.cpp                                \
    dependencies/PcapPlusPlus/Packet++/src/SingleCommandTextProtocol.cpp                        \
    dependencies/PcapPlusPlus/Packet++/src/TelnetLayer.cpp                                      \
    dependencies/PcapPlusPlus/Packet++/src/FtpLayer.cpp                                         \

#-----------------------------------------------------------------------------------------------#
# System dependencies                                                                           #
#-----------------------------------------------------------------------------------------------#
# linux
unix:LIBS += -lpcap

contains(QMAKE_PLATFORM, linux) {
    icon.files+=res/USBReverseHelper.png
}

# windows only

win32:LIBS += -L"$$PWD/dependencies/winpcap/Lib/x64" -lwpcap
win32:LIBS += -L"$$PWD/dependencies/winpcap/Lib/x64" -lPacket
win32:LIBS += -lws2_32
win32:INCLUDEPATH += dependencies/winpcap/Include

win32:RC_ICONS +=                                                                               \
    res/USBReverseHelper.ico

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res.qrc
